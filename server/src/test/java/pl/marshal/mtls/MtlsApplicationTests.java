package pl.marshal.mtls;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MtlsApplicationTests {

    @LocalServerPort
    private int serverPort;

    @Test
    public void contextLoads() throws Exception {
        // given
        String testUrl = "https://localhost:" + serverPort;
        RestTemplate restTemplate = new RestTemplate(createFactory());

        // when
        ResponseEntity<User> response = restTemplate.getForEntity(testUrl, User.class);

        // then
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody().getUsername()).isEqualTo("John Smith");
    }

    private ClientHttpRequestFactory createFactory() throws Exception {
        File clientCert = new File("src/main/resources/client.p12");
        char[] clientCertPass = {'6', '5', '4', '3', '2', '1'};
        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
                new SSLContextBuilder()
                        .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                        .loadKeyMaterial(clientCert, clientCertPass, clientCertPass)
                        .build()
        );
        HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
        return new HttpComponentsClientHttpRequestFactory(httpClient);
    }

    static class User {
        private String username;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
