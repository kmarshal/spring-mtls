package pl.marshal.mtls.client;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.security.KeyStore;

@SpringBootApplication
public class MtlsClientApplication implements CommandLineRunner {

	@Value("${mtls.server.url}")
	private String serverUrl;

	public static void main(String[] args) {
		SpringApplication.run(MtlsClientApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("RUN: " + serverUrl);

		RestTemplate restTemplate = new RestTemplate(sslFactory());

		ResponseEntity<String> response = restTemplate.getForEntity(serverUrl, String.class);
		System.out.println("Status: " + response.getStatusCodeValue());
	}

	private ClientHttpRequestFactory sslFactory() throws Exception {
		char[] clientCertPass = {'6', '5', '4', '3', '2', '1'};

		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(getClass().getClassLoader().getResourceAsStream("client.p12"), clientCertPass);

		KeyStore trustStore = KeyStore.getInstance("JKS");
		trustStore.load(getClass().getClassLoader().getResourceAsStream("trustStoreWithCA.jks"), "123456".toCharArray());

		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
				new SSLContextBuilder()
						.loadTrustMaterial(trustStore, new TrustSelfSignedStrategy())
						.loadKeyMaterial(keyStore, clientCertPass)
						.build()
		);
		HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
		return new HttpComponentsClientHttpRequestFactory(httpClient);
	}
}
