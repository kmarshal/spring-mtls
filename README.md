# README #

Reference:  
[https://gist.github.com/mtigas/952344](https://gist.github.com/mtigas/952344)  
[http://blog.nategood.com/client-side-certificate-authentication-in-ngi](http://blog.nategood.com/client-side-certificate-authentication-in-ngi)  
[http://www.baeldung.com/x-509-authentication-in-spring-security](http://www.baeldung.com/x-509-authentication-in-spring-security)  

### Create CA ###

```
openssl genrsa -des3 -out ca.key 4096
openssl req -new -x509 -days 365 -key ca.key -out ca.crt
```

### Create server certificate and sign with CA and export as pkcs12 ###
```
openssl genrsa -des3 -out server.key 1024
openssl req -new -key server.key -out server.csr
openssl x509 -req -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt
openssl pkcs12 -export -clcerts -in server.crt -inkey server.key -out server.p12
```

### Create client certificate and sign with CA ###
(WARNING ```-set_serial``` must be different than server one)
```
openssl genrsa -des3 -out client.key 1024
openssl req -new -key client.key -out client.csr
openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 02 -out client.crt
```
To use client cert in web browser need to create p12 certificate.
```
openssl pkcs12 -export -clcerts -in client.crt -inkey client.key -out client.p12
```

### Create jks containing our CA certificate ###
```
keytool -importcert -keystore trustStoreWithCA.jks -storepass 123456 -file ca.crt
```
### Test connection with curl ###
```
curl -v -s -k --key client.key --pass 654321 --cert client.crt https://localhost:8443
```


#### Passwords ####
* ca.key *123456*
* ca.p12 export password: *123456*
* server.key *123456*
* client.key *654321*
* client.p12 export password: *654321*